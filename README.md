# Moderne GitLab Ingest

## Intro

This CI configuration can be used to ingest repositories from GitLab into
the Moderne platform. This works by creating child pipelines from a csv file
containing minimal repository meta data.

## Setup

### Fork this project

This project will be responsible for ingesting a list of GitLab repositories into Moderne.

### Add/update the CSV file to the project

Add/update a CSV file to the project named `repositories.csv` with the following columns:

    - repoName - The name of the repository including an organization if applicable (e.g. moderneinc/moderne-gitlab-ingest)
    - branch - optional - The branch to ingest
    - style - optional - Force style to use for the repository
    - additionalBuildArgs - optional - Additional arguments to pass to the `mod build` command
    - skip - optional - Whether or not to skip the repository
    - skipReason - optional - The reason to skip the repository

### Add secrets

The most secure way to add secrets is
through [Vault Integration](https://docs.gitlab.com/ee/ci/secrets/index.html#use-vault-secrets-in-a-ci-job).
If you are already using this, we recommend you put your secrets in vault and adapt the pipeline in `.gitlab-ci.yml` to
use these.

The simpler (but [less secure](https://docs.gitlab.com/ee/ci/pipelines/pipeline_security.html#cicd-variables) approach)
is to use masked CI/CD variables:

1. Go to the **CI/CD** settings of the forked project
2. Expand the **Variables** section.
3. Click Add variable
4. Add a key and value (see [Update pipeline variables](#update-pipeline-variables) to find which ones to create)
5. Mask the variable to avoid it from being logged in the pipeline
6. Optional: Protect the variable to limit its use to only protected branches
7. Optional: Disable expand variable reference. This should not be needed

### Update pipeline variables

Update the variables on top op `.gitlab-ci.yml` with your:

    - PUBLISH_URL - the base url to your artifact repository
    - PUBLISH_USER_SECRET - the name of the (secret) variable containing the artifact storage username
    - PUBLISH_PASSWORD_SECRET - the name of the (secret) variable containing the artifact storage password
    - GITLAB_ACCESS_USER_SECRET - The name of the secret containing either a username (when using PAT) or the group name (when using group access token).
                                  It should be available to the ingest project. 
    - GITLAB_ACCESS_TOKEN_SECRET - The name of the secret containing either a PAT or group access token. It should be available to the ingest project.

**Note:** When filling in the above variables do NOT add actual passwords or variables containing passwords, but only
the names of the variables (so do not start with `$`)

Do this: `PUBLISH_USER_SECRET: MY_SECRET` <br>
Not this: `PUBLISH_USER_SECRET: $MY_SECRET` or `PUBLISH_USER_SECRET: p@ssw0rd

### Advanced configuration

If this configuration is not sufficient, these advanced options can be added to the mod-connect invocation:

    --commandSuffix - The suffix that should be appended to the Moderne CLI command when running GitLab jobs. Example `--Xmx 4g`
    --defaultBranch - Used when no branch is provided in the csv file.
    --downloadCLI=true - Can be added to download the CLI before the builds and passing it by cache. Useful when you need a later version
                         than is in the docker image or you host your own. Note that this only works reliably with a shared gitlab cache.
    --downloadCLIUrl - Can be added to download a specific or self-hosted version of the CLI.
    --downloadCLITokenSecret - Can be used when downloading the CLI form a custom URL with (Bearer) token bases authentication
    --downloadCLIUserNameSecret - The name of the secret containing a username for basic authentication used to download the CLI  
    --downloadCLIPasswordSecret - The name of the secret containing a password for basic authentication used to download the CLI
    --gradlePluginVersion - The version of the Gradle plugin to use when building the LST
    --mvnPluginVersion - The version of the Maven plugin to use when building the LST
    --mirrorUrl - For Gradle projects, this can be specified as a Maven repository cache/mirror to check before any other repositories.
    --prefix - If specified, GitLab jobs will only be created for repositories that start with this prefix.
    --skipSsl - If this parameter is included, SSL verification will be skipped when pushing to artifactory.
    --platform - The OS platform for the Gitlab runner. The possible options are: windows, linux, or macos. **Note:** only linux is well tested!
    --dockerImageBuildJob - The docker image to use when building LSTs. Requires a JDK and git and preferably the mod cli, unless downloading is enabled.
    --dockerImageDownloadJob - The docker image to use when downloading the CLI.
    --jobTag - Add a tag to both download and build child jobs if runners require this to pick up the jobs.
    --buildJobRetries - Retries to attempt for the build job. Options are: 0, 1 or 2.
    --verbose - If enabled, additional debug statements will be printed.

### Schedule ingest

To schedule the pipeline to ingest regularly follow the
steps [here](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

### Known issues

- The gitlab username should be valid for use in basic auth URL. Make sure to escape it in the secret.
